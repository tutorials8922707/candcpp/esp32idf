-- USER_STARTUP.restore_session_au()

local function load()
    vim.cmd("Lazy load overseer.nvim")

    local dap = require("dap")
    local overseer = require("overseer")

    vim.api.nvim_create_augroup("User_startup_autgroup_repo_filetype", { clear = true })
    vim.api.nvim_create_autocmd("BufReadPre", {
        group = "User_startup_autgroup_repo_filetype",
        pattern = { "*.h" },
        callback = function(args)
            vim.filetype.add({
                extension = {
                    h = "c",
                },
            })
        end,
    })

    overseer.register_template({
        name = "01 Flash and monitor",
        builder = function(params)
            return {
                cmd = { "idf.py" },
                args = { "flash", "monitor" },
            }
        end,
    })
    overseer.register_template({
        name = "02 Build",
        builder = function(params)
            return {
                cmd = { "idf.py" },
                args = { "build" },
            }
        end,
    })
    overseer.register_template({
        name = "03 Flash",
        builder = function(params)
            return {
                cmd = { "idf.py" },
                args = { "flash" },
            }
        end,
    })
end

vim.api.nvim_create_augroup("User_startup_autgroup_repo", { clear = true })
vim.api.nvim_create_autocmd("User", {
    group = "User_startup_autgroup_repo",
    pattern = "VeryLazy",
    callback = function()
        load()
    end,
})

#ifndef WIFI_APP_H
#define WIFI_APP_H

#include "esp_netif.h"
#include "esp_system.h"

// wifi application settings
#define WIFI_AP_SSID "ESP32_AP"
#define WIFI_AP_PASSWORD "password"
#define WIFI_AP_CHANNEL 1
#define WIFI_AP_SSID_HIDDEN 0
#define WIFI_AP_MAX_CONNECTIONS 1
#define WIFI_AP_BEACON_INTERVAL 100
#define WIFI_AP_IP "10.10.0.1"
#define WIFI_AP_GATEWAY "10.10.0.1"
#define WIFI_AP_NETMASK "255.255.255.0"
#define WIFI_AP_BANDWIDTH WIFI_BW_HT20
#define WIFI_STA_POWER_SAVE WIFI_PS_NONE
#define MAX_SSID_LENGHT 32
#define MAX_PASSWORD_LENGHT 64
#define MAX_CONNECTION_RETRIES 5

// netif object for the Station and Access point
extern esp_netif_t *esp_netif_sta;
extern esp_netif_t *esp_netif_ap;


/**
  * Message IDs for the WIFI application task
  * @note Expand this based on your application requirements.
  */
typedef enum wifi_app_message
{
    WIFI_APP_MSG_START_HTTP_SERVER = 0,
    WIFI_APP_MSG_CONNECTING_FROM_HTTP_SERVER = 1,
    WIFI_APP_MSG_STA_CONNECTED_GOT_IP = 2,
} wifi_app_message_e;

/**
  * Structure for message queue
  * @note Expand this based on application requirements e.g. add another type and parameter as requirement
  */
typedef struct wifi_app_queue_message
{
    wifi_app_message_e msgID;
} wifi_app_queue_message_t;

/**
  * Sends a message to the queue
  * @param  msgID message ID from the wifi_app_message_e enum.
  * @return pdTrue if an item was successfully sent to the queue, otherwise false
  * @note Expand the parameter list based on your requirements e.g. how you've expanded the wifi_app_queue_message_t.
  */
BaseType_t wifi_app_send_message(wifi_app_message_e messageID);


void wifi_app_start(void);


#endif
